<?php

class Deposit{


    /**
     * Holds deposit start date in Europe/Tallinn
     * @var \DateTime
     */
	private $date = null;


    /**
     * Holds deposit amount
     * @var float
     */
	private $amount = null;


    /**
     * Holds deposit interest rate
     * @var float
     */
	private $interest = null;


    /**
     * Holds deposit duration in months
     * @var int
     */
	private $months = null;
	
	
	/**
	 * Return counted schedule
	 * @return array
	 */
	public function getSchedule() : array
	{
		$schedule = [];

        $date = clone $this->getDate();
        for($m = 0; $m < $this->months; $m++) {
            $monthDays = $this->monthDays((int)$date->format('Y'), (int)$date->format('m'));
            $yearDays = $this->yearDays((int)$date->format('Y'));

            $interestCounted = round($this->amount * $this->interest / 100 / $yearDays * $monthDays, 2);

            $periodStart = $date->format("Y-m-d");
            $date->add(new DateInterval('P' . ($monthDays - 1) . 'D'));
            $periodEnd = $date->format("Y-m-d");
            $date->add(new DateInterval('P1D'));

            $amountStart = $this->amount;
            $this->amount = round($this->amount + $interestCounted, 2);

            $schedule[] = [
                "periodStartDate" => $periodStart,
                "periodEndDate" => $periodEnd,
                "interestCounted" => $interestCounted,
                "amountStart" => $amountStart,
                "amountEnd" => $this->amount,
            ];
        }

		return $schedule;		
	}
	
	
	/**
	 * Set deposit start date
	 *
	 * - Date should be in local Europe/Tallinn timezone
	 * 
	 * @param \DateTime $date
	 * @return Deposit
	 */
	public function setDate( \DateTime $date ) : Deposit
	{
        $date->setTimeZone(new DateTimeZone('Europe/Tallinn'));
		$this->date = $date;
		return $this;
	}
	
	
	/**
	 * Get deposit start date
	 * @return \DateTime
	 */
	public function getDate() : \DateTime
	{
		return $this->date;
	}
	
	
	/**
	 * Set deposit amount
	 *
	 * - Amount should be rounded to 2 decimal places
	 * - Possible range: [1000.00;10000.00]
	 * - If it is out of bounds, throw an exception
	 * 
	 * @param float $amount
	 * @return Deposit
	 */
	public function setAmount( float $amount) : Deposit
	{
        $amount = round($amount, 2);
        if ($amount < 1000.0 || $amount > 10000.0) {
            throw new Exception("Amount is out of bounds", 6969);
        }
		$this->amount = $amount;
		return $this;
	}
	
	
	/**
	 * Get deposit amount
	 * @return float
	 */
	public function getAmount() : float
	{
		return (float)$this->amount;
	}
	
	
	/**
	 * Set deposit duration in months
	 *
	 * - Possible range: [1;36]
	 * - If it is out of bounds, assign to closest possible value (1 or 36)
	 * 
	 * @param int $months
	 * @return Deposit
	 */
	public function setMonths( int $months ) : Deposit
	{
	    if ($months < 1) $months = 1;
	    else if ($months > 36) $months = 36;
		$this->months = $months;
		return $this;
	}
	
	
	/**
	 * Get deposit duration in months
	 * @return int
	 */
	public function getMonths() : int
	{
		return (int)$this->months;
	}
	
	
	/**
	 * Set deposit interest rate
	 *
	 * - Interest should be rounded to 2 decimal places
	 * - Possible range: [6.00;12.00]
	 * - If it is out of bounds, throw an exception
	 * 
	 * @param float $interest
	 * @return Deposit
	 */
	public function setInterest( float $interest ) : Deposit
	{
	    $interest = round($interest, 2);
	    if ($interest < 6.0 || $interest > 12.0) {
	       throw new Exception("Interest is out of bounds", (int)($interest * 100));
        }
		$this->interest = round($interest, 2);
		return $this;
	}
	
	
	/**
	 * Get deposit interest rate
	 * @return float
	 */
	public function getInterest() : float
	{
		return (float)$this->interest;
	}


    /**
     * Returns is year leap or not
     * @param int $year
     * @return bool
     */
	private function isLeapYear(int $year) : bool {
        return (0 == $year % 4) && (0 != $year % 100) || (0 == $year % 400);
    }


    /**
     * Returns number of days in month
     * @param int $year
     * @param int $month
     * @return int
     */
	private function monthDays(int $year, int $month) : int {
        return $month == 2 ? ($this->isLeapYear($year) ? 29 : 28) : (($month - 1) % 7 % 2 ? 30 : 31);
    }


    /**
     * Returns number of days in year
     * @param int $year
     * @return int
     */
	private function yearDays(int $year) : int {
        return $this->isLeapYear($year) ? 366 : 365;
    }
	
}

